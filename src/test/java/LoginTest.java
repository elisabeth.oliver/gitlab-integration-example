/* Assignment: Build a testNG project including Applitools and Page Object Model

    Testing: http://the-internet.herokuapp.com/login

    Planned Tests:
    - Successful Log in
    - Successful Log out
    - Unsuccessful Log in
 */

import com.applitools.eyes.*;
import com.applitools.eyes.selenium.BrowserType;
import com.applitools.eyes.selenium.Configuration;
import com.applitools.eyes.selenium.Eyes;
import com.applitools.eyes.selenium.fluent.Target;
import com.applitools.eyes.visualgrid.services.RunnerOptions;
import com.applitools.eyes.visualgrid.services.VisualGridRunner;
import org.apache.commons.lang3.NotImplementedException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.*;
import pageObjects.LoginPage;
import pageObjects.SecurePage;

import java.lang.reflect.Method;

public class LoginTest {
    WebDriver driver;
    static BatchInfo batchInfo;
    Eyes eyes;
    EyesRunner runner;


    @BeforeSuite
    public void openBatch(){
        batchInfo = new BatchInfo("GitLab Integration Demo");
        batchInfo.setSequenceName("GitLab Integration Demo");
        batchInfo.setNotifyOnCompletion(true);
        batchInfo.addProperty("Purpose", "Demo");


        runner = new VisualGridRunner(new RunnerOptions().testConcurrency(5));

        //runner = new ClassicRunner();

        driver = getDriver("chrome-headless");
    }

    @BeforeMethod
    public void eyesOpen(Method method) {
        eyes = new Eyes(runner);
        setEyesConfig(eyes, true);

        eyes.open(driver, "The Internet", method.getName());
    }

    private WebDriver getDriver(String browser) {
        WebDriver driver;
        switch (browser.toLowerCase().replaceAll("\\s", "")) {
            case "chrome":
                driver = new ChromeDriver();
                break;
            case "chrome-headless":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                driver = new ChromeDriver(options);
                break;
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "internetexplorer":
            case "ie":
                driver = new InternetExplorerDriver();
            case "edge":
                driver = new EdgeDriver();
            default:
                throw new NotImplementedException(
                        "Chosen browser (" + browser + ") is currently not supported by this test.");
        }

        return driver;
    }

    private void setEyesConfig(Eyes eyes, Boolean crossBrowser) {
        Configuration configuration = eyes.getConfiguration();
        configuration.setBatch(batchInfo)
                .setViewportSize(new RectangleSize(800, 600))
                .setForceFullPageScreenshot(true);


        //eyes.addProperty("Custom Meaningful Tag!", getProperty());

        if (crossBrowser) {
            configuration.addBrowser(800, 600, BrowserType.CHROME);
//            configuration.addBrowser(1600, 1200, BrowserType.CHROME);
//            configuration.addBrowser(700, 500, BrowserType.FIREFOX);
//            configuration.addBrowser(1600, 1200, BrowserType.IE_11);
//            configuration.addBrowser(1024, 768, BrowserType.EDGE_CHROMIUM);
//            configuration.addBrowser(800, 600, BrowserType.SAFARI);
////
//            configuration.addDeviceEmulation(DeviceName.iPhone_X, ScreenOrientation.PORTRAIT);
//            configuration.addDeviceEmulation(DeviceName.Pixel_2, ScreenOrientation.PORTRAIT);
//            configuration.addBrowser(new IosDeviceInfo(IosDeviceName.iPad_Air_2));
        }

        eyes.setConfiguration(configuration);
    }

    @Test
    public void badLogin(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.navigate();

        loginPage.submitCredentials("tomsmith", "not the password");
        eyes.check("login:fail - incorrect password", Target.window());

        loginPage.submitCredentials("not the username", "SuperSecretPassword!");
        eyes.check("login:fail - incorrect username", Target.window());

        loginPage.submitCredentials("not the username", "not the password");
        eyes.check("login:fail - incorrect username and password", Target.window());
    }

    @Test
    public void loginLogout(){

        LoginPage loginPage = new LoginPage(driver);
        loginPage.navigate();

        SecurePage page = loginPage.submitCredentials("tomsmith", "SuperSecretPassword!");
        makeMistake();
        eyes.check("login:successful", Target.window());


        page.logout();
        eyes.check("logout", Target.window());
    }

    public void makeMistake() {
        driver.findElement(By.cssSelector("#flash"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('flash').style.color = \"#5da423\";");
    }

    public void makeBlue() {
        driver.findElement(By.cssSelector("#flash"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('flash').style.color = \"blue\";");
    }

    String getProperty(){
        String[] list = new String[]{"A", "B", "C"};
        int num = (int)Math.floor(Math.random()*list.length);

        return list[num];
    }

    @AfterMethod
    public void closeEyes() {
        eyes.closeAsync();
    }

    @AfterSuite
    public void closeBatch(){
        driver.close();
        //runner.getAllTestResults();

        TestResultsSummary allTestsresults = runner.getAllTestResults();
        TestResultContainer[] results = allTestsresults.getAllResults();
        for (int i=0; i<results.length; i++){
            String testUrl = results[i].getTestResults().getUrl();

            System.out.println(testUrl);
        }
    }
}
