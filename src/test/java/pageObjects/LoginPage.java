package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends PageObject {
    private String url = "http://the-internet.herokuapp.com/login";

    By userNameFieldSelector = By.cssSelector("#username");
    By passwordFieldSelector = By.cssSelector("#password");
    By credentialFormSelector = By.cssSelector("#login");
    By loginButtonSelector = By.cssSelector("button.radius");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void navigate() {
        driver.get(url);
    }

    public SecurePage submitCredentials(String username, String password) {
        WebElement userNameField = driver.findElement(userNameFieldSelector);
        WebElement passwordField = driver.findElement(passwordFieldSelector);
        WebElement credentialForm = driver.findElement(credentialFormSelector);

        userNameField.sendKeys(username);
        passwordField.sendKeys(password);
        credentialForm.submit();

        return new SecurePage(driver);
    }

    public void hideButton(){
        WebElement button = driver.findElement(loginButtonSelector);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].remove();", button);
    }
}
