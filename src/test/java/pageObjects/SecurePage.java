package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SecurePage extends PageObject {

    By logoutButtonSelector = By.cssSelector(".button");

    public SecurePage(WebDriver driver){
        super(driver);

    }

    public LoginPage logout(){
        WebElement logoutButton = driver.findElement(logoutButtonSelector);

        logoutButton.click();

        return new LoginPage(driver);
    }
}
